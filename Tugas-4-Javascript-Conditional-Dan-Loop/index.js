//SOAL 1
var nilai = 59;

if (nilai>=85) {
    console.log('Indeksnya A')
}
else if (nilai>=75 && nilai<85) {
    console.log('Indeksnya B')
}
else if (nilai>=65 && nilai<75) {
    console.log('Indeksnya C')
}
else if (nilai>=55 && nilai<65) {
    console.log('Indeksnya D')
}
else {
    console.log('Indeksnya E')
}


//SOAL 2
var tanggal = 14;
var bulan = 8;
var tahun = 2001;

switch (bulan) {
    case 1: { bulan='Januari'; break; }
    case 2: { bulan='Februari'; break; }
    case 3: { bulan='Maret'; break; }
    case 4: { bulan='April'; break; }
    case 5: { bulan='Mei'; break; }
    case 6: { bulan='Juni'; break; }
    case 7: { bulan='Juli'; break; }
    case 8: { bulan='Agustus'; break; }
    case 9: { bulan='September'; break; }
    case 10: { bulan='Oktober'; break; }
    case 11: { bulan='November'; break; }
    case 12: { bulan='Desember'; break; }
    default: { bulan='-'; }
}
if (bulan=='-'){
    console.log('Bulan Salah');
}
else {
    console.log(tanggal + ' ' + bulan + ' ' + tahun)
}


//SOAL 3
var n = 7
var segitiga='';

for (var baris=1; baris <= n; baris++) {
    for (var kolom=0; kolom < baris; kolom++) {
        segitiga += '*'
    }
    segitiga += '\n'
}
console.log(segitiga)


//SOAL 4
var m = 10
var kata1 = 'I Love Programming'
var kata2 = 'I Love Javascript'
var kata3 = 'I Love VueJS'
var samaDengan = ''

for(var i = 1; i <= m; i++) {
    samaDengan += '='
    if ((i+2)%3==0){
        console.log(i+' - '+kata1);
    }
    else if ((i+1)%3==0){
        console.log(i+' - '+kata2);
    }
    else if (i%3==0){
        console.log(i+' - '+kata3);
        console.log(samaDengan)
    }
    
}
