//SOAL 1

var tanggal = 31
var bulan = 12
var tahun = 2021

function next_date(tanggal, bulan, tahun){
    tanggal += 1

    if (tahun%4 == 0 || tahun%400 == 0){
            if (bulan == 2){
                if (tanggal > 29){
                    tanggal = 1
                    bulan += 1
                    if (bulan > 12){
                        bulan = 1
                        tahun += 1
                    }  
                }
            }
    }  
    else {
        if (bulan == 1 || bulan == 3 || bulan == 5 || bulan == 7 || bulan == 8 || bulan == 10 || bulan == 12){
            if (tanggal > 31){
                tanggal = 1
                bulan += 1
                if (bulan > 12){
                    bulan = 1
                    tahun += 1
                }  
            }
        }
        else if (bulan == 4 || bulan == 6 || bulan == 9 || bulan == 11){
            if (tanggal > 30){
                tanggal = 1
                bulan += 1
                if (bulan > 12){
                    bulan = 1
                    tahun += 1
                }    
            }
        }
        else if (bulan == 2){
            if (tanggal > 28){
                tanggal = 1
                bulan += 1
                if (bulan > 12){
                    bulan = 1
                    tahun += 1
                }    
            }
        }
    }
    switch(bulan) {
        case 1:   { bulan = 'Januari'; break; }
        case 2:   { bulan = 'Februari'; break; }
        case 3:   { bulan = 'Maret'; break; }
        case 4:   { bulan = 'April'; break; }
        case 5:   { bulan = 'Mei'; break; }
        case 6:   { bulan = 'Juni'; break; }
        case 7:   { bulan = 'Juli'; break; }
        case 8:   { bulan = 'Agustus'; break; }
        case 9:   { bulan = 'September'; break; }
        case 10:   { bulan = 'Oktober'; break; }
        case 11:   { bulan = 'November'; break; }
        case 12:   { bulan = 'Desember'; break; }
    }
    return tanggal+' '+bulan+' '+tahun
}

var soal_1 = next_date(tanggal, bulan, tahun)
console.log(soal_1)

//SOAL 2
var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok "
var kalimat_2 = " Saya Iqbal"

function jumlah_kata(kalimat){
    var koreksi_kalimat = kalimat.trim()
    var kata = 0
    for (var i = 0; i < koreksi_kalimat.length; i++){
        if(koreksi_kalimat[i]==' '){
            kata += 1
        }
        
    }
    kata = kata + 1
    return kata
}

var soal_2 = jumlah_kata(kalimat_1)
console.log(soal_2)

 