//SOAL 1
const keliling_luas = (panjang, lebar) => {
    const keliling = (panjang+lebar)*2;
    const luas = panjang*lebar;
    const hasil = `Keliling :: ${keliling} \nLuas :: ${luas}`
    console.log(hasil)
}
keliling_luas(40, 20);


//SOAL 2
const newFunction = (firstName, lastName) => {
    const fullName = () => {
        console.log(`${firstName} ${lastName}`)
    }
    return {firstName, lastName, fullName}
  }
   
//Driver Code 
newFunction("William", "Imoh").fullName() 


//SOAL 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}

const {firstName, lastName, address, hobby} = newObject
// Driver code
console.log(firstName, lastName, address, hobby)


//SOAL 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
//Driver Code
console.log(combined)


//SOAL 5
const planet = "earth" 
const view = "glass" 
const before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`

console.log(before)